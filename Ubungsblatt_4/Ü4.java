import java.sql.Connection;
import java.sql.DriverManager;

//die Datei postgresql-42.1.1.jar ist notwendig, um verbinden zu k�nnen

public class PostgreSQLBeispiel 
{
   public static void main(String args[]) 
   {
       //Verbindung Objekt erstallen
      Connection c;
      try {
         Class.forName("org.postgresql.Driver");
         //Verbindung zum Server/Datenbank aufbauen
         //die Parameter sind:
         //1) Adress der Datenbank auf unserem lokalen Server
         //2) Benutzername
         //3) Passwort
         c = DriverManager.getConnection("jdbc:postgresql://localhost:8050/dbs","testuser", "testpass");
          } 
      catch (Exception e) {
         e.printStackTrace();
      }
   }
}