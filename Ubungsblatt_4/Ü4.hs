module Main where

-- Imports
import Database.PostgreSQL.Simple
import Control.Monad
import Control.Applicative

main = do
  -- erstelle Verbindung zur Datenbank
  conn <- connect defaultConnectInfo {connectDatabase = "dbs"}

  -- query ausf�hren
  execute conn "SELECT * FROM Student"