# zu erst die Libraries installieren: pip install psycopg2

#dann importieren
import psycopg2

# Verbindung zur Datenbank erstellen
conn = psycopg2.connect("dbname=dbs user=testuser password=testpass")

# Curser erstellen, um queries ausfuehren zu koennen
cur = conn.cursor()

#query ausfuehren
cur.execute("SELECT * FROM Student;")
cur.fetchall()
