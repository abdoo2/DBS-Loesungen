-- Start des Programmes, main muss existieren
main = do  
    -- Frage den Benutzer nach seinem Namen
    putStrLn "Wie heißt du?"  
    -- Speichere den eingegebenen Namen in der Variable nameDesBenutzers
    nameDesBenutzers <- getLine  
    -- Drucke den Namen in der richtigen Form
    putStrLn ("Hallo " ++ nameDesBenutzers ++ " !") 