//importiere Scanner, um Benutzereingabe zu "fangen"
import java.util.Scanner;

//start des Programms
public class Hallo
{
    public static void main (String[] args)
    {
        //neuen Scanner erstellen
        Scanner inputScanner = new Scanner(System.in);
        //frage nach dem Namen
        System.out.println("wie heißt du?");
        //Benutzereingabe in Variable speichern
        String nameDesBenutzers = inputScanner.nextLine();
        //drucke den Namen in der richtingen Form
        System.out.println("Hallo " + nameDesBenutzers + " !");
    }
}