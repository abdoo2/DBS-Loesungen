# imports
from bs4 import BeautifulSoup
import requests

# das ist ein Kommentar
def getPage(url):
    r = requests.get(url)
    data = r.text
    spobj = BeautifulSoup(data, "lxml")
    return spobj

# scraper website: heise.de
def getHeaders():

    https_links = ["https://www.heise.de/thema/https", 
                   "https://www.heise.de/thema/https?seite=1", 
                   "https://www.heise.de/thema/https?seite=2",
                   "https://www.heise.de/thema/https?seite=3"]
    
    header_array = []
    
    for link in https_links:
      content = getPage(link).find("div", { "class" : "keywordliste"})
      content = content.find_all("header")
      content = content[1:]
      for c in content:
        title = str(c)
        #cut useless stuff away
        title = title[8:-27]
        header_array.append(title)

    print("\nDONE !\n\n\n Heise.de/thema/https was scraped completely.\n")  
    return header_array

#find most common word
def findCommonWord(headers):
  words = []
  for header in headers:
    words = words + header.lower().split()
    
  # Get the set of unique words.
  uniques = []
  for word in words:
    if word not in uniques:
      uniques.append(word)
  
  # Make a list of (count, unique) tuples.
  counts = []
  for unique in uniques:
    count = 0              # Initialize the count to zero.
    for word in words:     # Iterate over the words.
      if word == unique:   # Is this word equal to the current unique?
        count += 1         # If so, increment the count
    counts.append((count, unique))
  
  counts.sort()            # Sorting the list puts the lowest counts first.
  counts.reverse()         # Reverse it, putting the highest counts first.
  
  numWords = 3             # Number of Words with the highest count to be printed
  # Print the THREE words with the highest counts.
  for i in range(min(numWords, len(counts))):
    count, word = counts[i]
    print('%s %d' % (word, count))
  
  
# main program

if __name__ == '__main__':
    findCommonWord(getHeaders())