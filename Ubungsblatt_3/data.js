var data =
"02.01.17,109.30,110.80,110.30,2330417;\
03.01.17,110.11,112.09,110.68,882614;\
04.01.17,110.53,111.70,111.03,449615;\
05.01.17,109.40,110.73,110.00,746100;\
06.01.17,109.80,112.00,111.61,700163;\
09.01.17,111.60,113.10,112.46,1297576;\
10.01.17,111.80,112.90,112.75,1691504;\
11.01.17,111.83,113.86,112.17,1521071;\
12.01.17,110.94,112.60,111.76,1073743;\
13.01.17,111.60,112.50,111.76,1149538;\
16.01.17,111.72,112.45,112.00,861942;\
17.01.17,110.00,112.29,112.08,848050;\
18.01.17,112.00,112.70,112.35,528968;\
19.01.17,111.99,113.09,112.71,778900;\
20.01.17,112.14,113.10,112.14,610841;\
23.01.17,111.04,112.50,111.74,366744;\
24.01.17,110.90,111.73,111.51,734456;\
25.01.17,111.70,113.65,113.65,1221101;\
26.01.17,113.20,114.38,114.00,1302214;\
27.01.17,113.70,114.46,114.03,739897;\
30.01.17,112.90,113.99,113.35,1268146;\
31.01.17,111.78,113.50,111.91,988174"



//////////////////////////////
var datum           = []
var tief            = []
var hoch            = []
var tagesendwert    = []
var handelsvolumen  = []
//////////////////////////////
//Die Werte in den enstprechenden Arrays speichern
//Linien sind mit ; separiert
var lines  = data.split(";");

for (var i = 0; i < lines.length; i++) {
  //Werte sind mit , speariert
  var x = lines[i].split(",");
  datum.push(convertTime(x[0]));
  tief.push(parseFloat(x[1]));
  hoch.push(parseFloat(x[2]));
  tagesendwert.push(parseFloat(x[3]));
  handelsvolumen.push(parseInt(x[4]));
}


//Plot erstelen
function plot () {

  //Line-Chart-Grafik
  $.plot("#linechart",
        [ zip([datum, tief]), zip([datum, hoch]), zip([datum, tagesendwert]) ],
        {
          xaxis:{mode: "time", timeformat: "%m/%d"},
          colors:["#ff0000","#00ff00","#ffff00"]
        });

  //Bar-Chart-Grafik
  $.plot("#barchart",
        [ zip([datum, handelsvolumen]) ],
        {
          xaxis:{mode: "time",timeformat: "%m/%d"},
          series: {bars:{show: true, align: "center", barWidth: 1000*60*60*24}}
        });
}

//zip funktion, um Tupeln aus den Daten zu erstellen
function zip(arrays) {
    return arrays[0].map(function(_,i){
        return arrays.map(function(array){return array[i]})
    });
};

//der standard von JS ist mm/dd/yyyy
//die Funktion ändert unsere Einträge
function convertTime(date) {
  var dateParts = date.split(".");
  return (new Date(dateParts[2], dateParts[1] - 1, dateParts[0])); // month ist 0-basiert
}
